import { Injectable } from '@angular/core';
declare var hello: any;

@Injectable({
  providedIn: 'root'
})
export class LinkedinService {

  hello = hello;
  OAUTH_PROXY_URL = {
    'local.knarly.com' : 'http://local.knarly.com:5500/proxy'
  }[ 'linkedin' ] || 'https://auth-server.herokuapp.com/proxy';

  constructor() {
    console.log('hello', hello);
  }

  init(id, columns) {
    hello.init({
      'linkedin' : id,
    },{
      scope : [columns.split(',')],
      redirect_uri: 'http://loginwithlinkedin-jr.herokuapp.com/index2.php',
      oauth_proxy: this.OAUTH_PROXY_URL
    });


    hello.on('auth.login', function(r){
      // Get Profile
      hello(r.network).api('me').then(function(p){
        console.log("p", p);
        // document.getElementById('login').innerHTML = "<img src='"+ p.thumbnail + "' width=24/>Connected to "+ r.network+" as " + p.name;
      });
    });
  }

  login() {
    hello.login('linkedin');
  }
}
